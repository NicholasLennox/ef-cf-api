﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFOsloClassDemo.Models;
using EFOsloClassDemo.Models.Domain_Models;

namespace EFOsloClassDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachesController : ControllerBase
    {
        private readonly TrainerDbContext _context;

        public CoachesController(TrainerDbContext context)
        {
            _context = context;
        }

        // GET: api/Coaches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Coach>>> GetCoaches()
        {
            return await _context.Coaches.ToListAsync();
        }

        // GET: api/Coaches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Coach>> GetCoach(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);

            if (coach == null)
            {
                return NotFound();
            }

            return coach;
        }

        // PUT: api/Coaches/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, Coach coach)
        {
            if (id != coach.Id)
            {
                return BadRequest();
            }

            _context.Entry(coach).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoachExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Coaches
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Coach>> PostCoach(Coach coach)
        {
            _context.Coaches.Add(coach);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCoach", new { id = coach.Id }, coach);
        }

        // DELETE: api/Coaches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCoach(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);
            if (coach == null)
            {
                return NotFound();
            }

            _context.Coaches.Remove(coach);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CoachExists(int id)
        {
            return _context.Coaches.Any(e => e.Id == id);
        }

        // Some extra things

        // 1. Get certifications for coach
        // 2. Add certifications to coach
        // 3. Update existing certifications for coach
        [HttpGet("{id}/certifications")]
        public async Task<ActionResult<IEnumerable<Certification>>> GetCertsForCoach(int id)
        {
            Coach coach =  await _context.Coaches.Include(c => c.Certifications).Where(c => c.Id == id).FirstOrDefaultAsync();
            if (coach == null)
            {
                return NotFound();
            }
            foreach (Certification certification in coach.Certifications)
            {
                certification.Coaches = null;
            }

            return coach.Certifications.ToList();
        }

        [HttpPost("{id}/certifications")]
        public async Task<IActionResult> AddCertsToCoach(int id, List<int> certificaitons)
        {
            Coach coach = await _context.Coaches.Include(c => c.Certifications).FirstOrDefaultAsync(c => c.Id == id);
            if (coach == null)
            {
                return NotFound();
            }

            foreach (int certid in certificaitons)
            {
                if(coach.Certifications.FirstOrDefault(c => c.Id == certid) == null)
                {
                    Certification cert = await _context.Certifications.FindAsync(certid);
                    if(cert != null)
                    {
                        coach.Certifications.Add(cert);
                    }
                }
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpPut("{id}/certifications")]
        public async Task<IActionResult> UpdateCertsForCoach(int id, List<int> certificaitons)
        {
            Coach coach = await _context.Coaches.Include(c => c.Certifications).FirstOrDefaultAsync(c => c.Id == id);
            if (coach == null)
            {
                return NotFound();
            }

            coach.Certifications.Clear();

            foreach (int certid in certificaitons)
            {
                Certification cert = await _context.Certifications.FindAsync(certid);
                coach.Certifications.Add(cert);
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }


    }
}
